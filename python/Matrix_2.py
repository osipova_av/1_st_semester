import numpy as np
import matplotlib.pyplot as plt
import random
import math

T = 0.8

n = 6
matrix = np.ones((n, n))
nums = [-1, 1]
for i in range(n):
    for j in range(n):
        matrix[i][j] = random.choice(nums)
print('Initial matrix:')
print(matrix)

E1 = 0
for i in range(n):
    for j in range(n):
        E1 += matrix[i][j] * matrix[i][(j+1)%n]   
for i in range(n):
    for j in range(n):
        E1 += matrix[j][i] * matrix[(j+1)%n][i]
print('Initial sum =',E1)

for x in range(300):
    index = [0, 1, 2, 3, 4, 5]
    k = random.choice(index)
    m = random.choice(index)
    random_elem = matrix[k][m]
    matrix[k][m] = random_elem * (-1)
    #print('Choisen element:', random_elem)
    #print('Modified matrix:')
    #print(matrix)
    
    E2 = 0
    for i in range(n):
        for j in range(n):
            E2 += matrix[i][j] * matrix[i][(j+1)%n]   
    for i in range(n):
        for j in range(n):
            E2 += matrix[j][i] * matrix[(j+1)%n][i]
            
    del_E = E2 - E1
    #print('Sum in the modified matrix =', E2)
    #print('System energy change =', del_E)
         
    if del_E <= 0:
        E1 = E2
    else:
        P = random.random()
        if P == 0:
            while P == 0:
                P = random.random()
                if P != 0:
                    break
        W = math.exp(-del_E / T)
        if P <= W:
            E1 = E2
        else:
            matrix[k][m] = random_elem

E = 0
for i in range(n):
    for j in range(n):
        E += matrix[i][j] * matrix[i][(j+1)%n]     
for i in range(n):
    for j in range(n):
        E += matrix[j][i] * matrix[(j+1)%n][i]
print('Final configuration:')
print(matrix)
print('Final value E =', E)

plt.imshow(matrix)