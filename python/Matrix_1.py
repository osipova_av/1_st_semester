import numpy as np
import random
n = int(input("matrix size: "))
a = np.ones((n, n))
nums = [-1, 1]
for i in range(n):
    for j in range(n):
        a[i][j] = random.choice(nums)
print(a)
summ = 0
for i in range(n):                     
    for j in range(n):                 
          summ += a[i][j]*a[i][(j+1)%n]    
            
for j in range(n):                      
    for i in range(n):
        summ += a[j][i]*a[(j+1)%n][i]
print("sum:", summ)
