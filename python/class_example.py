import random

class Human():
    
    default_name = 'No Name'
    default_age = 0
    
    def __init__(self, name = default_name, age = default_age):
        points = [True, False]
        self.name = name
        self.age = age
        self.__money = 0
        self.__house = None
        self.driving = random.choice(points)
        print(f"{self.name}'s life has started!")
    
    def info(self):
        print(f'Name: {self.name}')
        print(f'Age: {self.age}')
        print(f'Money: {self.__money}')
        print(f'House: {self.__house}')
        
    def driving(self):
        if self.driving:
            print(f'{self.name} can drive a car')
        else:
            print(f"{self.name} can't drive a car")
        
    def earn_money(self, salary):
        self.__money += salary
        print(f'Earned {salary} money! Current value: {self.__money}')
        
    def buy_house(self, house, discount):
        price = house.final_price(discount)
        if self.__money >= price:
            self.__make_business(house, price)
        else:
            print("Not enough money to buy a house. Earn money")
    
    def __make_business(self, house, price):
        self.__money -= price
        self.__house = house
        
    def estate(self):
        if self.driving == True:
            print(f'{self.name} has a car')
        else:
            print(f"{self.name} hasn't a car")
            
        
class House():
    
    def __init__(self, area, price):
        self._area = area
        self._price = price
    
    def final_price(self, discount):
        final_price = self._price * (100 - discount) / 100 
        print(f'Final price: {final_price}')
        return final_price 
    
class Confortable_house(House):
    
    default_area = 100
    
    def __init__(self, price):
        super().__init__(Confortable_house.default_area, price)
    
        
person = Human('Ivan', 32)
person.info()
comfortable_house = Confortable_house(7000)
person.buy_house(comfortable_house, 5)
person.earn_money(8000)
person.buy_house(comfortable_house, 5)
person.info()
person.estate()