#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd                                                                  #пункт 2

cols = [ 'Name' , 'Gender', 'Birth']
years = range(1880, 2011)
pieces = []
for i in years:
    df = pd.read_table('yob%d.txt'%i, sep = ',', engine = 'python' , names = cols)
    df['Year'] = i
    pieces.append(df)
    data = pd.concat(pieces, ignore_index = True)
    
data


# In[44]:


num_girls = data.loc[(data['Gender'] == 'F'), 'Birth'].sum()               #пункт 1
num_boys = data.loc[(data['Gender'] == 'M'), 'Birth'].sum()
num_babies = num_girls + num_boys
print('Общее число родившихся девочек:', num_girls)
print('Общее число родившихся мальчиков:', num_boys)
print('Общее число родившихся младенцев:', num_babies)


# In[35]:


for i in years:
    print("Количество девочек, родившихся в", i, ":", data.loc[(data['Gender'] == 'F') & (data['Year'] == i), 'Birth'].sum())              
    print("Количество мальчиков, родившихся в", i,":", data.loc[(data['Gender'] == 'M') & (data['Year'] == i), 'Birth'].sum())
    print()


# In[34]:


import matplotlib.pyplot as plt                                                        #пункт 3
plt.style.use('seaborn-whitegrid')

x1 = []
x2 = []
for i in years:
    x1.append(data.loc[(data['Gender'] == 'F') & (data['Year'] == i), 'Birth'].sum())
    x2.append(data.loc[(data['Gender'] == 'M') & (data['Year'] == i), 'Birth'].sum())
    
fig, ax = plt.subplots()

y = range(1880, 2011)

ax.plot(y, x1, color = 'pink', linestyle = '-', label = 'Рождаемость девочек')
ax.plot(y, x2, color = 'blue', linestyle = '-', label = 'Рождаемость мальчиков')

ax.set_title('Рождаемость детей с 1880 по 2010', fontsize = 20)

ax.set_xlabel('Год')
ax.set_ylabel('Количество новорожденных')

ax.legend(loc = 'lower right')


# In[29]:


data['Proportion'] = data['Birth'] / num_babies                      #пункт 4
data


# In[32]:


x_1 = []                                                                                   #пункт 5(общее количество младенцев)
x_2 = []
x_3 = []
x_4 = []
for i in years:
    x_1.append(data.loc[(data['Name'] == 'Johnny') & (data['Year'] == i), 'Birth'].sum())
    x_2.append(data.loc[(data['Name'] == 'Natalie') & (data['Year'] == i), 'Birth'].sum())
    x_3.append(data.loc[(data['Name'] == 'Bob') & (data['Year'] == i), 'Birth'].sum())
    x_4.append(data.loc[(data['Name'] == 'Anastasia') & (data['Year'] == i), 'Birth'].sum())
    
fig, ax = plt.subplots()

y = range(1880, 2011)

ax.plot(y, x_1, color = 'b', linestyle = '-', label = 'Количество мальчиков с именем  Johny')
ax.plot(y, x_2, color = 'violet', linestyle = '-', label = 'Количество девочек с именем Natalie')
ax.plot(y, x_3, color = 'g', linestyle = '-', label = 'Количество мальчиков с именем Bob')
ax.plot(y, x_4, color = 'coral', linestyle = '-', label = 'Количество девочек с именем Anastasia')

ax.set_title('Общее количество младенцев, получивших определенное имя с 1880 по 2010', fontsize = 20)

ax.set_xlabel('Год')
ax.set_ylabel('Количество новорожденных')

ax.legend(loc = 'upper left')


# In[33]:


y_1 = []                                                                                 #пункт 5 (относительная доля младнцев)
y_2 = []
y_3 = []
y_4 = []
for i in years:
    y_1.append(data.loc[(data['Name'] == 'Johnny') & (data['Year'] == i), 'Proportion'].sum())
    y_2.append(data.loc[(data['Name'] == 'Natalie') & (data['Year'] == i), 'Proportion'].sum())
    y_3.append(data.loc[(data['Name'] == 'Bob') & (data['Year'] == i), 'Proportion'].sum())
    y_4.append(data.loc[(data['Name'] == 'Anastasia') & (data['Year'] == i), 'Proportion'].sum())
    
fig, ax = plt.subplots()

y = range(1880, 2011)

ax.plot(y, y_1, color = 'b', linestyle = '-', label = 'Количество мальчиков с именем  Johny')
ax.plot(y, y_2, color = 'violet', linestyle = '-', label = 'Количество девочек с именем Natalie')
ax.plot(y, y_3, color = 'g', linestyle = '-', label = 'Количество мальчиков с именем Bob')
ax.plot(y, y_4, color = 'coral', linestyle = '-', label = 'Количество девочек с именем Anastasia')

ax.set_title('Относительная доля младенцев, получивших определенное имя с 1880 по 2010', fontsize = 20)

ax.set_xlabel('Год')
ax.set_ylabel('Количество новорожденных')

ax.legend(loc = 'upper left')


# In[68]:


#поскольку частота встречаемости каждого имени в каждом файле отсортирована в порядке возрастания, можем          пункт 6
#взять талько первую строку
for i in years:                                                     
    print("Самое популярное имя в", i,':', data.loc[(data['Birth'].max()) & (data['Year'] == i),'Name'].iloc[0])              


# In[ ]:




