#!/usr/bin/env python
# coding: utf-8

# In[47]:


import pandas as pd
import numpy as np

unames = ['UserID','Gender','Age','Occupation','Zip-code']
users = pd.read_table('users.dat', sep = '::', engine = 'python', names = unames)

rnames = ['UserID','MovieID','Rating','Timestamp']
ratings = pd.read_table('ratings.dat', sep = '::', engine = 'python', names = rnames)

mnames = ['MovieID','Title','Genres']
movies = pd.read_table('movies.dat', sep = '::', engine = 'python', names = mnames)

data = pd.merge(users, ratings)
data = pd.merge(data, movies)
data


# In[16]:


mean_ratings_gender = pd.pivot_table(data, 'Rating', index = 'Title', columns = 'Gender', aggfunc = 'mean') #пункт 3
mean_ratings_gender


# In[17]:


mean_ratings_age = pd.pivot_table(data, 'Rating', index = 'Title', columns = 'Age', aggfunc = 'mean')  #пункт 3
mean_ratings_age


# In[22]:


mean_ratings_gender.sort_values(by = 'F', ascending = False)                                 #пункт 4


# In[25]:


mean_ratings_age.sort_values(by = 56, ascending = False)                                     #пункт 4


# In[63]:


rating_title = data.groupby('Title')                                               #пункт 5
rating_title.mean()


# In[49]:


mean_ratings_gender['Difference'] = mean_ratings_gender['F'] - mean_ratings_gender['M']     #пункт 6
s = mean_ratings_gender.sort_values(by = 'Difference', ascending = False)
s.head(15)


# In[38]:


mean_ratings_gender['Difference'] = mean_ratings_gender['M'] - mean_ratings_gender['F']        # пункт 6
s = mean_ratings_gender.sort_values(by = 'Difference', ascending = False)
s.head(15)


# In[ ]:




